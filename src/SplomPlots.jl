module SplomPlots

using Plots
using DataFrames

function splom(df::DataFrame; group="", columns=[])
    if columns == []
        columns = Dict(names(df) .=> eltype.(eachcol(df)))
    else
        columns = Dict(
            names(df[!, columns]) .=>
                eltype.(eachcol(df[!, columns])),
        )
    end

    for (key, value) in columns
        if value <: Number
            continue

        else
            pop!(columns, key)
        end
    end

    cols = collect(keys(columns))
    col_pairs = [(x, y) for x in cols, y in cols]
    col_len = length(cols)
    scatter_plots = repeat(
        [scatter([1], [1])], length(col_pairs)
    )

    for (i, (x, y)) in enumerate(col_pairs)
        scatter_plots[i] = plot()

        if x == y
            if i == col_len^2 && group != ""
                fakedat = (1:length(df[!, x])) .* 0
                scatter!(
                    fakedat,
                    fakedat;
                    xaxis=nothing,
                    yaxis=nothing,
                    showaxis=false,
                    grid=false,
                    xlims=(1, 0),
                    ylims=(1, 0),
                    group=df[!, group],
                    legend=:topleft,
                )
            else
                plot!(;
                    xaxis=nothing,
                    yaxis=nothing,
                    showaxis=false,
                )
            end
        else
            if group != ""
                scatter!(
                    df[!, x],
                    df[!, y];
                    group=df[!, group],
                    label="",
                )
            else
                scatter!(df[!, x], df[!, y]; label="")
            end
        end

        if i % col_len == 1
            ylabel!(y)
        end

        if length(col_pairs) - i < col_len
            xlabel!(x)
        end
    end

    return plot(scatter_plots...;)
end

export splom

end # module
