# SplomPlots.jl

`S`catter`plo`t `M`atrix inspired by [Plotly](https://blog.plotly.com/post/174657459542/what-is-a-splom-chart-making-scatterplot-matrices) for Plots.jl

## Usage

```julia
using Pkg; Pkg.add(url="https://gitlab.com/MisterBiggs/splomplots.jl.git")

using SplomPlots

# Load in a dataset
using RDatasets
df = dataset("datasets", "iris")

# Create a splom from the dataset using the species column for grouping the scatter plots
splom(df; group=:Species)
```

![Example Splom from Iris dataset](examples/iris.png)
