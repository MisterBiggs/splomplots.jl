using Test

using SplomPlots

using DataFrames
# using Plots
using RDatasets
# gr()
# plotlyjs()
using Plots
pyplot()
# pgfplotsx()
# unicodeplots()

df = dataset("datasets", "iris")

# splom(
#     df;
#     group=:Species,
#     columns=[:SepalLength, :SepalWidth, :PetalWidth],
# )
# splom(df)
splom(df; group=:Species)

df = dataset("datasets", "trees")
splom(df)

df = dataset("ggplot2", "diamonds")
splom(
    df[1:500:end, :];
    group=:Cut,
    columns=[:Carat, :Depth, :Price],
)

@test 1 == 1

df = dataset("MASS", "Aids2")
splom(df[completecases(df), :])
describe(df)
splom(
    df;
    group=:Fail,
    columns=[:Temperature, :Pressure, :Damage],
)
splom(df; columns=[:War, :Ver, :Fly, :Hai])
splom(df)
splom(df; group=:Status)
